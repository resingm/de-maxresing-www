#!/bin/bash

deploy_msg='automatic deployment'

srv=${1:-remote.maxresing.de}
dir=${2:-/var/www/www.maxresing.de/html/}

declare -a arr=(about.html contact.html index.html projects.html)

echo Commit "$deploy_msg"
git add .
git commit -m "$deploy_msg"
git push

echo Copying files to $srv...
for f in ${arr[@]}
do
	scp $f $srv:$dir
done

echo Copied ${#arr[@]} files sucessfully.

exit 0
